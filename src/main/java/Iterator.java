public interface Iterator {
    boolean hasNext();
    FootballPlayer next();
}
