package Stream;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Author {
    String name;
    String lastName;
    int yearOfBirth;
    String citizenship;

    public Author(String name, String lastName, int yearOfBirth, String citizenship) {
        this.name = name;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
        this.citizenship = citizenship;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                ", citizenship='" + citizenship + '\'' +
                '}';
    }
}

class Book {
    String title;
    String type;
    Author author;
    int yearOfRelease;

    public Book(String title, String type, Author author, int yearOfRelease) {
        this.title = title;
        this.type = type;
        this.author = author;
        this.yearOfRelease = yearOfRelease;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", author=" + author +
                ", yearOfRelease=" + yearOfRelease +
                '}';
    }
}


public class Main {
    public static void main(String[] args) {

        Author Lem = new Author("Stanisław", "Lem", 1921, "polskie");
        Author Dick = new Author("Philip K","Disck",1928, "amerykańskie");

        List<Book> shelf = Arrays.asList(   new Book("Solaris", "S-F", Lem, 1961),
                                            new Book("Bajki robotów", "S-F", Lem, 1964),
                                            new Book("Śledztwo", "Kryminał", Lem, 1959),
                                            new Book("Pełzacze", "S-F", Dick, 1974));


        // lista wszystkich tytułów książek
        shelf.stream()
                .map((Book book) -> {return book.title;})
                .collect(Collectors.toList())
                .forEach((String st) -> System.out.println(st));

        System.out.println();

        // lista wszstkich typów
        shelf.stream()
                .map((Book book) -> {return book.type;})
                .collect(Collectors.toSet())    //takie sprytne zastosowanie Seta
                .forEach((String st) -> System.out.println(st));

        System.out.println();

        // Ksiązki wydane po 1962
        shelf.stream()
                .filter((Book book) -> {return book.yearOfRelease > 1962;})
                .forEach((Book book) -> System.out.println(book));

        System.out.println();

        // lista wszstkich autorów
        shelf.stream()
                .map((Book book) -> {return book.author;})
                .collect(Collectors.toSet())    //kolejne sprytne zastosowanie Seta
                .forEach((Author au) -> System.out.println(au));

        System.out.println();

        // lista wszstkich autorów mających więcej niż 95 lat (mam na półce samą klasykę)
        shelf.stream()
                .map((Book book) -> {return book.author;})
                .filter((Author au) -> {return au.yearOfBirth < 1924;})
                .collect(Collectors.toSet())    //kolejne sprytne zastosowanie Seta
                .forEach((Author au) -> System.out.println(au));

        System.out.println();

        //Lista autorów z polskim obywatelstwem
        shelf.stream()
                .map((Book book) -> {return book.author;})
                .filter((Author au) -> {return au.citizenship.equalsIgnoreCase("polskie");})
                .collect(Collectors.toSet())    //kolejne sprytne zastosowanie Seta
                .forEach((Author au) -> System.out.println(au));

        System.out.println();

        //lista wszystkich Autorów wydających S-F, urodzonych po 1925
        shelf.stream()
                .filter((Book book) -> {return book.type.equalsIgnoreCase("s-f");})
                .map((Book book) -> {return book.author;})
                .filter((Author au) -> {return au.yearOfBirth > 1925;})
                .collect(Collectors.toSet())    //kolejne sprytne zastosowanie Seta
                .forEach((Author au) -> System.out.println(au));

        System.out.println();

        //lista wszystkich autorów którzy wydali książkę po 1962.
        shelf.stream()
                .filter((Book book) -> {return book.yearOfRelease > 1962;})
                .map((Book book) -> {return book.author;})
                .collect(Collectors.toSet())    //kolejne sprytne zastosowanie Seta
                .forEach((Author au) -> System.out.println(au));
    }
}