import java.util.List;

class FootballPlayerIterator implements Iterator{
    List <FootballPlayer> listOfPlayers;
    private int position;

    public FootballPlayerIterator(List <FootballPlayer> listOfPlayers) {
        this.listOfPlayers = listOfPlayers;
    }
    @Override
    public boolean hasNext() {
        return position  < listOfPlayers.size();
    }
    @Override
    public FootballPlayer next() {
        if (this.hasNext()) {
            return listOfPlayers.get(position++);
        }
        return null;
    }
}

