public class PizzaPepperoni extends PizzaDecorator {

    public PizzaPepperoni(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void createPizza() {
        super.createPizza();
        System.out.println("with pepperoni");
    }
}
