package Budowniczy;

import java.util.ArrayList;
import java.util.List;

public abstract class PizzaMaker {

    protected boolean thinBase = true;
    protected String cheese = "mozzarella";
    protected String sauce;
    protected List<Vegetable> vegetables = new ArrayList<>();
    protected String meat = "bacon";
    protected String sausage;

    public PizzaMaker setCheese(String cheese){
        this.cheese = cheese;
        return this;
    }

    public PizzaMaker isBaseThin(boolean isThin){
        this.thinBase = isThin;
        return this;
    }

    public PizzaMaker setSauce(String sauce){
        if(sauce == null) sauce = "red tomato sauce";
        this.sauce = sauce;
        return this;
    }

    public PizzaMaker addVegetable(Vegetable vegateble){
        this.vegetables.add(vegateble);
        return this;
    }

    public PizzaMaker setMeat(String meat){
        this.meat = meat;
        return this;
    }

    public PizzaMaker setSausage(String sausage){
        this.sausage = sausage;
        return this;
    }

    public abstract Pizza build();
}