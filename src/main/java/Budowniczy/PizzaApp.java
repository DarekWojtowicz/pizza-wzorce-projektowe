package Budowniczy;

public class PizzaApp {

    public static void main(String[] args) {

        Pizza myPizza = new PizzaPeruga()
                .isBaseThin(true)
                .setSausage("salami")
                .setSauce("gearlic")
                .addVegetable(Vegetable.PEPPER)
                .addVegetable(Vegetable.ONION)
                .build();

        System.out.println(myPizza);

    }
}
