package Budowniczy;



public class PizzaPeruga extends PizzaMaker {



    @Override
    public Pizza build() {
        return new Pizza(thinBase, cheese, sauce, vegetables, meat, sausage);
    }
}
