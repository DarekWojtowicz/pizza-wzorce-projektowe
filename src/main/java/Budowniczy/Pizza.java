package Budowniczy;

import java.util.ArrayList;
import java.util.List;

public class Pizza {

    private boolean thinBase;
    private String cheese;
    private String sauce;
    private List<Vegetable> vegetables = new ArrayList<>();
    private String meat;
    private String sausage;


    @Override
    public String toString() {
        return "Pizza{" +
                "pizzaBase='" + thinBase + '\'' +
                ", cheese='" + cheese + '\'' +
                ", sauce='" + sauce + '\'' +
                ", vegetables=" + vegetables +
                ", meat='" + meat + '\'' +
                ", sausage='" + sausage + '\'' +
                '}';
    }

    public boolean getPizzaBase() {
        return thinBase;
    }

    public void setPizzaBase(boolean thinBase) {
        this.thinBase = thinBase;
    }

    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public List<Vegetable> getVegetables() {
        return vegetables;
    }

    public void setVegetables(List<Vegetable> vegetables) {
        this.vegetables = vegetables;
    }

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getSausage() {
        return sausage;
    }

    public void setSausage(String sausage) {
        this.sausage = sausage;
    }

    public Pizza(boolean thinBase, String cheese, String sauce, List<Vegetable> vegetables, String meat, String sausage) {
        this.thinBase = thinBase;
        this.cheese = cheese;
        this.sauce = sauce;
        this.vegetables = vegetables;
        this.meat = meat;
        this.sausage = sausage;
    }
}
