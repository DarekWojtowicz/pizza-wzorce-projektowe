package test;

public class Main {
    public static void main(String[] args) {

        System.out.println("tworzyny B jako A");
        ClassA a = new ClassB();

        a.setAge1();
        a.setAge2();
        a.setAge3();
        a.setAge4();
        a.setAge5();

        System.out.println("tworzyny A jako A");
        ClassA b = new ClassA();

        b.setAge1();
        b.setAge2();
        b.setAge3();
        b.setAge4();
        b.setAge5();

        System.out.println("tworzyny B jako B");
        ClassB c = new ClassB();

        c.setAge1();
        c.setAge2();
        c.setAge3();
        c.setAge4();
        c.setAge5();
        c.setAge6();


//        System.out.println("tworzyny A jako B z rzutowaniem na B");
//        ClassB d = (ClassB) new ClassA();
//
//        d.setAge1();
//        d.setAge2();
//        d.setAge3();
//        d.setAge4();
//        d.setAge5();
//        d.setAge6();

        long at = 32416120943L;
        long startTime = System.nanoTime();
        System.out.println(Sum.isPrime(at));
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime / 1000000 + " milisekund");
    }
}
// 1579470352
//  785739549