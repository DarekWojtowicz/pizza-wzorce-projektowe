package test;

import java.util.stream.IntStream;

public class Sum {
    public static boolean isPrime(long num) {

        if(num < 1) {
            return false;
        }else {
            for(long i = 3; i < num; i+=2) {
                if(num % i == 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
