package Obserwator;

import java.time.LocalDate;
import java.util.Objects;

public class Promotion {
    String productName;
    double price;
    LocalDate expirationDate;

    public Promotion(String productName, double price, LocalDate expirationDate) {
        this.productName = productName;
        this.price = price;
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "Promotion{" +
                "productName='" + productName + '\'' +
                ", price=" + price +
                ", expirationDate=" + expirationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Promotion)) return false;
        Promotion promotion = (Promotion) o;
        return Double.compare(promotion.price, price) == 0 &&
                Objects.equals(productName, promotion.productName) &&
                Objects.equals(expirationDate, promotion.expirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productName, price, expirationDate);
    }
}
