package Obserwator;

public class EmailSubscriber implements Subscriber {

    private String email;

    public EmailSubscriber(String email) {
        this.email = email;
    }

    @Override
    public void notifyPromotion(String shopName, Promotion promotion) {
        System.out.println("Wiadomość o nowej promocji. Odbiorca: " + email + "\t" + shopName + "\t" + promotion );
    }
}
