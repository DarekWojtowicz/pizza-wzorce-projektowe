package Obserwator;

import java.util.ArrayList;
import java.util.List;

public class MediaMarkt implements SubscribedShops {

    private String agencyName;
    private List<Subscriber> subscribers = new ArrayList<>();
    private List<Promotion> promoList = new ArrayList<>();

    public MediaMarkt() {
        agencyName = "MediaMarkt";
    }

    @Override
    public void addSubscriber(Subscriber subscriber) {
        if (!subscribers.contains(subscriber)) {
            subscribers.add(subscriber);
        }
    }

    @Override
    public void removeSubscriber(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }

    @Override
    public void addPromotion(Promotion promotion) {
        this.promoList.add(promotion);
        announcePromotion(promotion);
    }

    private void announcePromotion(Promotion promotion) {
        for (Subscriber subscriber : subscribers) {
            subscriber.notifyPromotion(agencyName, promotion);
        }
    }
}
