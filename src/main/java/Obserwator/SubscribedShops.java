package Obserwator;

public interface SubscribedShops {
    void addSubscriber(Subscriber subscriber);
    void removeSubscriber(Subscriber subscriber);
    void addPromotion(Promotion promotion);
}
