package Obserwator;

public interface Subscriber {
    void notifyPromotion(String shopName, Promotion promotion);
}
