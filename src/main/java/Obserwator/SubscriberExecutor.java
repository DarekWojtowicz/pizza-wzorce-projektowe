package Obserwator;

import java.time.LocalDate;
import java.time.Month;

public class SubscriberExecutor {
    public static void main(String[] args) {

        Promotion promo1 = new Promotion("Laptop HP", 2200, LocalDate.of(2019, Month.AUGUST, 1));
        Promotion promo2 = new Promotion("iPhone8", 5200, LocalDate.of(2019, Month.AUGUST, 7));

        SubscribedShops mediaMarkt = new MediaMarkt();
        Subscriber s1 = new EmailSubscriber("mail1@mail.pl");
        Subscriber s2 = new EmailSubscriber("dw@poczta.pl");
        Subscriber s3 = new EmailSubscriber("sda@sda.pl");

        mediaMarkt.addSubscriber(s1);
        mediaMarkt.addSubscriber(s2);
        mediaMarkt.addSubscriber(s3);

        mediaMarkt.addPromotion(promo1);
        mediaMarkt.removeSubscriber(s3);
        mediaMarkt.addPromotion(promo2);
    }
}
