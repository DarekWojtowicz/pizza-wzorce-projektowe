package MetodaWytworcza;

public enum ButtonType {

    Submit("Submit button"),
    Radio("Radio button"),
    Check("Check box button");

    private String description;

    ButtonType(String description) {
        this.description = description;
    }
}
