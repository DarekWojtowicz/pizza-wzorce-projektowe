package MetodaWytworcza;

public interface ButtonFactory {
    Button getButton(ButtonType type);
}
