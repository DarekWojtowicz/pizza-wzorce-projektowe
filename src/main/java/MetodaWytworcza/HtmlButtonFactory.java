package MetodaWytworcza;

public class HtmlButtonFactory implements ButtonFactory {

    @Override
    public Button getButton(ButtonType type) {
        Button button = null;
        switch (type){
            case Submit :
                button = new SubmitButton();
                break;
            case Radio :
                button = new RadioButton();
                break;
            case Check :
                    button = new CheckBoxButton();
                break;
        }
        return null;
    }
}
