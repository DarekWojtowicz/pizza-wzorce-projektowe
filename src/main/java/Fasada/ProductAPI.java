package Fasada;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProductAPI {
    List <Product> lista = new ArrayList<>();
    Scanner sc = new Scanner(System.in);

    List<Product> getProducts() {
        return lista;
    }

    Product getProductByName(String name){
        for(Product pr : lista){
            if (pr.getName().equals(name)){
                System.out.println("Sukces!");
                return pr;
            }else {
                System.out.println("Brak towaru o takiej nazwie!");
                return null;
            }
        }return null;
    };

    void addProduct(){
        String name;
        String description;
        float price;

        System.out.println("DODAWANIE PRODUKTÓW - proszę o uważne wprowadzane -  brak kontroli typów");
        System.out.print("Podaj nazwe nowego produktu : ");
        name = sc.nextLine();
        System.out.print("Podaj opis nowego produktu : ");
        description = sc.nextLine();
        System.out.print("Podaj cenę : ");
        price = sc.nextFloat();

        Product product = new Product(name, description, price);
        System.out.println();
        System.out.println("Produkt dodany poprawnie");
        lista.add(product);
    };

    void removeProduct(String name){
        for(int i = 0; i < lista.size();i++){
            if (lista.get(i).getName().equals(name)){
                System.out.println("Usuwam towar!");
                lista.remove(i);
            }else {
                System.out.println("Brak towaru o takiej nazwie!");
            }
        }
    };
}
