package Fasada;

import java.util.List;
import java.util.Scanner;

public class ShopAPI {
    private PaymentAPI paymentAPI = new PaymentAPI();
    private UserAPI userAPI = new UserAPI();
    private ProductAPI productAPI = new ProductAPI();
    Scanner sc = new Scanner(System.in);

    void register(String login, String password){
        userAPI.register(login, password);
    };
    void login(String login, String password){
        userAPI.login(login, password);
    };
    void logout(){
        userAPI.logout();
    };
    void buyProduct(Product product){
        productAPI.getProductByName(product.getName());
        paymentAPI.pay(product.getPrice());
    };
    void addProduct(){
        productAPI.addProduct();
    };
    void removeProduct(){
        System.out.print("Produkt do usunięcia: ");
        productAPI.removeProduct(sc.nextLine());
    };
    void listaProduktow(){
        List<Product> lista;
        lista = productAPI.getProducts();
    }
}
