package Notebook;

public class Off implements LaptopStatus {

    @Override
    public void next(Laptop laptop) {
        laptop.setStatus(new Hibernate());
    }

    @Override
    public void prev(Laptop laptop) {
        System.out.println("This status is unavaible!");
    }

    @Override
    public void printStatus() {
        System.out.println("Off!");

    }
}
