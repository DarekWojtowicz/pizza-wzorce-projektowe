package Notebook;

public class LaptopExecutor {
    public static void main(String[] args) {

        Laptop hp = new Laptop();
        hp.printStatus();

        hp.prevState();
        hp.printStatus();

        hp.prevState();
        hp.printStatus();

        hp.prevState();
        hp.printStatus();

        hp.nextState();
        hp.printStatus();

        hp.nextState();
        hp.printStatus();

        hp.nextState();
        hp.printStatus();

        hp.nextState();
        hp.printStatus();
    }
}
