package Notebook;

public class Sleep implements LaptopStatus {

    @Override
    public void next(Laptop laptop) {
        laptop.setStatus(new On());
    }

    @Override
    public void prev(Laptop laptop) {
        laptop.setStatus(new Hibernate());
    }

    @Override
    public void printStatus() {
        System.out.println("Sleep!");
    }
}
