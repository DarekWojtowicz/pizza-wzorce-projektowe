package Notebook;

public interface LaptopStatus {

    void next(Laptop laptop);
    void prev(Laptop laptop);
    void printStatus();

}
