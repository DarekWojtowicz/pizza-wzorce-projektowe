package Notebook;

public class Laptop {
    private LaptopStatus status = new Sleep();

    public void setStatus(LaptopStatus status) {
        this.status = status;
    }

    public void nextState() {
        status.next(this);
    };

    public void prevState(){
        status.prev(this);
    }

    public void printStatus(){
        status.printStatus();

    }
}
