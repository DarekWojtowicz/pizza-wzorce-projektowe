package Notebook;

public class Hibernate implements LaptopStatus {

    @Override
    public void next(Laptop laptop) {
        laptop.setStatus(new Sleep());
    }

    @Override
    public void prev(Laptop laptop) {
        laptop.setStatus(new Off());
    }

    @Override
    public void printStatus() {
        System.out.println("Hibernate!");
    }
}
