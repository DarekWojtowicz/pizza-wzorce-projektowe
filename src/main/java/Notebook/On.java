package Notebook;

public class On implements LaptopStatus {

    @Override
    public void next(Laptop laptop) {
        System.out.println("This status is unavaible!");
    }

    @Override
    public void prev(Laptop laptop) {
        laptop.setStatus(new Sleep());
    }

    @Override
    public void printStatus() {
        System.out.println("On!");
    }
}
