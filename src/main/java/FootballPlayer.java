public class FootballPlayer {
    private String name;
    private String surname;
    private Position position;
    private int skill;

    public FootballPlayer(String name, String surname, Position position, int skill) {
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.skill = skill;
    }

    public FootballPlayer(String name, String surname, int skill) {
        this.name = name;
        this.surname = surname;
        this.skill = skill;
    }

    public FootballPlayer(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return  "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", " + position +
                ", skill=" + skill +
                '}';
    }
}
