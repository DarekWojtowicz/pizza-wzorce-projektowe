package Memento;

import java.util.Stack;

public class LibreOffice {
    public static void main(String[] args) {

        Document doc = new Document("Mój pierwszy dokumemnt", FontColour.BLACK, 12);
        Stack<MementoDocument> stack = new Stack<>();
        System.out.println(doc);
        final MementoDocument m = doc.createMemento();
        stack.push(m);
        doc.setColour(FontColour.INDIAN_RED);
        final MementoDocument m2 = doc.createMemento();
        stack.push(m2);
        doc.setFontSize(44);
        System.out.println(doc);
        doc.restore(stack.pop());
        System.out.println(doc);
        doc.restore(stack.pop());
        System.out.println(doc);


    }
}
