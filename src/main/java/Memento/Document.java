package Memento;

public class Document {

    private String text;
    private FontColour colour;
    private int fontSize;

    public Document(String text, FontColour colour, int fontSize) {
        this.text = text;
        this.colour = colour;
        this.fontSize = fontSize;
    }

    public MementoDocument createMemento()
    {
        MementoDocument m = new MementoDocument(text, colour, fontSize);
        return m;
    }

    public void restore(MementoDocument m) {
        this.text = m.getText();
        this.colour = m.getColour();
        this.fontSize = m.getFontSize();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public FontColour getColour() {
        return colour;
    }

    public void setColour(FontColour colour) {
        this.colour = colour;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    @Override
    public String toString() {
        return "Document{" +
                "text='" + text + '\'' +
                ", colour=" + colour +
                ", FontSize=" + fontSize +
                '}';
    }
}
