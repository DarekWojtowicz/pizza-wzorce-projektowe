package Memento;

public class MementoDocument {

    private final String text;
    private final FontColour colour;
    private final int fontSize;

    public MementoDocument(String text, FontColour colour, int fontSize) {
        this.text = text;
        this.colour = colour;
        this.fontSize = fontSize;
    }

    public String getText() {
        return text;
    }

    public FontColour getColour() {
        return colour;
    }

    public int getFontSize() {
        return fontSize;
    }
}
