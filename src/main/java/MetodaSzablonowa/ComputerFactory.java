package MetodaSzablonowa;

public class ComputerFactory {
    public static void main(String[] args) {
        ComputerOffer comp1 = new GamingComputer();
        ComputerOffer comp2 = new OfficeComputer();
        ComputerOffer comp3 = new DeveloperComputer();

        Computer c1 = comp1.CreateComputer();
        comp2.CreateComputer();
        comp3.CreateComputer();

        System.out.println(c1);

    }
}
