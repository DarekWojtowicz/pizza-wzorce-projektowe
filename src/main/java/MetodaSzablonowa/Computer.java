package MetodaSzablonowa;

public class Computer {
    private String processor;
    private String motherboard;
    private int ram;
    private boolean isSSD;
    private String diskSpace;
    private String graphicCard;
    private OS  operationSystem;

    public Computer() {

    }

    public Computer(String processor, String motherboard, int ram, boolean isSSD, String diskSpace, String graphicCard, OS operationSystem) {
        this.processor = processor;
        this.motherboard = motherboard;
        this.ram = ram;
        this.isSSD = isSSD;
        this.diskSpace = diskSpace;
        this.graphicCard = graphicCard;
        this.operationSystem = operationSystem;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(String motherboard) {
        this.motherboard = motherboard;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public boolean isSSD() {
        return isSSD;
    }

    public void setSSD(boolean SSD) {
        isSSD = SSD;
    }

    public String getDiskSpace() {
        return diskSpace;
    }

    public void setDiskSpace(String diskSpace) {
        this.diskSpace = diskSpace;
    }

    public String getGraphicCard() {
        return graphicCard;
    }

    public void setGraphicCard(String graphicCard) {
        this.graphicCard = graphicCard;
    }

    public OS getOperationSystem() {
        return operationSystem;
    }

    public void setOperationSystem(OS operationSystem) {
        this.operationSystem = operationSystem;
    }

    @Override
    public String toString() {
        return "Processor='" + processor + '\'' +
                ", motherboard='" + motherboard + '\'' +
                ", ram=" + ram +
                ", isSSD=" + isSSD +
                ", diskSpace='" + diskSpace + '\'' +
                ", graphicCard='" + graphicCard + '\'' +
                ", operationSystem=" + operationSystem +
                '}';
    }
}
