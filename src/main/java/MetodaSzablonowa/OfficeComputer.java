package MetodaSzablonowa;

public class OfficeComputer extends ComputerOffer {
    Computer computer = new Computer();

    @Override
    public String installProcessor() {
        computer.setProcessor("Intel Atom");
    return "ee";
    }

    @Override
    public void installMatherBoard() {
        computer.setMotherboard("Asus - super wypasiona plyta dla graczy");
    }

    @Override
    public void installRam() {
        computer.setRam(4);
    }

    @Override
    public void ssdOrNot() {
        computer.setSSD(false);
    }

    @Override
    public void installDiskWithChoosenSpace() {
        computer.setDiskSpace("500GB");
    }

    @Override
    public void installGrapgicCard() {
        computer.setGraphicCard("Zintegrowawana - najsłąbsza karta na rynku");
    }

    @Override
    public void installSystem() {
        computer.setOperationSystem(OS.UBUNTU);
    }
}
