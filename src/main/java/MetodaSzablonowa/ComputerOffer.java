package MetodaSzablonowa;

public abstract class ComputerOffer {

    //     final public Computer CreateComuter(String processor, String matherBoard, int ram, boolean ssd, String diskSpace, String graphicCard, String system) {
//         Computer computer = new Computer(processor, matherBoard, ram, ssd, diskSpace, graphicCard, OS.valueOf(system));
//         return computer;
//     }
    final public Computer CreateComputer() {
        Computer computer = new Computer();

        computer.setProcessor(installProcessor());
        installMatherBoard();
        installRam();
        ssdOrNot();
        installDiskWithChoosenSpace();
        installGrapgicCard();
        installSystem();

        return computer;
    }

    public abstract String installProcessor();

    public abstract void installMatherBoard();

    public abstract void installRam();

    public abstract void ssdOrNot();

    public abstract void installDiskWithChoosenSpace();

    public abstract void installGrapgicCard();

    public abstract void installSystem();

}
