package MetodaSzablonowa;

public class GamingComputer extends ComputerOffer {
    Computer computer;

    @Override
    public String installProcessor() {
        computer.setProcessor("i7");
        return "rr";
    }

    @Override
    public void installMatherBoard() {
        computer.setMotherboard("Asus - super wypasiona plyta dla graczy");
    }

    @Override
    public void installRam() {
        computer.setRam(16);
    }

    @Override
    public void ssdOrNot() {
        computer.setSSD(true);
    }

    @Override
    public void installDiskWithChoosenSpace() {
        computer.setDiskSpace("1TB");
    }

    @Override
    public void installGrapgicCard() {
        computer.setGraphicCard("Nvidia GeForce RTX 2070");
    }

    @Override
    public void installSystem() {
        computer.setOperationSystem(OS.WIN10);
    }
}
