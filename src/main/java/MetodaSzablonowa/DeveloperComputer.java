package MetodaSzablonowa;

public class DeveloperComputer extends ComputerOffer {
    Computer computer = new Computer();


//    @Override
//    public void installProcessor() {
//        computer.setProcessor("i7");
//    }

    @Override
    public String installProcessor() {
        return null;
    }

    @Override
    public void installMatherBoard() {
        computer.setMotherboard("Asus - super wypasiona plyta dla graczy");
    }

    @Override
    public void installRam() {
        computer.setRam(8);
    }

    @Override
    public void ssdOrNot() {
        computer.setSSD(true);
    }

    @Override
    public void installDiskWithChoosenSpace() {
        computer.setDiskSpace("1TB");
    }

    @Override
    public void installGrapgicCard() {
        computer.setGraphicCard("Nvidia GeForce RTX 1050");
    }

    @Override
    public void installSystem() {
        computer.setOperationSystem(OS.OSX);
    }
}
