public class Main {

    public static void main(String[] args) {

        String a = "255.255.255.255";
        String pattern = "(([0-1]\\d{0,2})\\.|(2[0-4]?[0-9]?\\.|(25[0-5]?\\.))){3}(([0-1]\\d{0,2})|(2[0-4]?[0-9]?)|(25[0-5]?))";
        System.out.println(a.matches(pattern));
    }
}
