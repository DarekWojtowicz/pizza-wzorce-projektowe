package Stan;

public class Vibracje implements PhoneVolumeState {
    @Override
    public void next(Phone phone) {
        phone.setState(new Dzwiek());
    }

    @Override
    public void prev(Phone phone) {
        phone.setState(new Wyciszenie());

    }

    @Override
    public void printStatus() {
        System.out.println("Włączono : tryb WIBRACJE");

    }
}
