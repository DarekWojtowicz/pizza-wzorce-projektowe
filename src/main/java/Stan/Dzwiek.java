package Stan;

public class Dzwiek implements PhoneVolumeState {
    @Override
    public void next(Phone phone) {
        System.out.println("Nie ma dalszego trybu");
    }

    @Override
    public void prev(Phone phone) {
        phone.setState(new Vibracje());
    }

    @Override
    public void printStatus() {
        System.out.println("Włączono : tryb DZWIĘK");
    }
}
