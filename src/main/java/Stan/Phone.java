package Stan;

public class Phone {
    private PhoneVolumeState state = new Wyciszenie();

    public void setState(PhoneVolumeState state) {
        this.state = state;
    }

    public void previousState() {
        state.prev(this) ;
    }

    public void nextState() {
        state.next(this);
    }

    public void printStatus() {
        state.printStatus();
    }
}
