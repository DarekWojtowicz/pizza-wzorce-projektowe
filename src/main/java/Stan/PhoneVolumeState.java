package Stan;

public interface PhoneVolumeState {

    void next(Phone phone);
    void prev(Phone phone);
    void printStatus();
}
