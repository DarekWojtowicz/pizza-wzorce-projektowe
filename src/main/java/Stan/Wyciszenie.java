package Stan;

public class Wyciszenie implements PhoneVolumeState {
    @Override
    public void next(Phone phone) {
        phone.setState(new Vibracje());
    }

    @Override
    public void prev(Phone phone) {
        System.out.println("Nie ma dalszego trybu");
    }

    @Override
    public void printStatus() {
        System.out.println("Włączono : tryb WYCISZENIE");
    }
}
