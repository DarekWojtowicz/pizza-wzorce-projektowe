package Mediator;

import java.util.ArrayList;
import java.util.List;

public class TaxiDispatcher implements Dispatcher {

    private List<Driver> drivers = new ArrayList<>();

    @Override
    public void newOrder(String order) {
        for(Driver driver : drivers) {
            driver.onNewOrder(order);
        }
    }

    @Override
    public void addDriver(Driver driver) {
        drivers.add(driver);
    }
}
