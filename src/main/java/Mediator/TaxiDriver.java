package Mediator;

public class TaxiDriver extends Driver {

    private Dispatcher dispatcher;

    @Override
    public void onNewOrder(String orderName) {
        System.out.println(name + " Taxi driver received order");
    }

    public TaxiDriver(Dispatcher dispatcher, String name) {
        super(name);
        this.dispatcher = dispatcher;

    }
}
