package Mediator;

public abstract class Driver {
    protected String name;

    public Driver(String name) {
        this.name = name;
    }

    public void onNewOrder(String orderName) {
        System.out.println(name + " " + orderName);
    };
}
