package Mediator;

public interface Dispatcher {
    public void newOrder(String order);
    public void addDriver(Driver driver);
}
