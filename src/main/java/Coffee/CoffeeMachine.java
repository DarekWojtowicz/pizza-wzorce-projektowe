package Coffee;

public class CoffeeMachine {

    private CoffeeMachineStatus status = new Ready();

    public void setStatus(CoffeeMachineStatus status) {
        this.status = status;
    }

    public void previousState() {
        status.prev(this);

    }
    public void nextState() {
        status.next(this);

    }
    public void printStatus() {
        status.printStatus();
    }
}
