package Coffee;

public class Ready implements CoffeeMachineStatus{
    @Override
    public void next(CoffeeMachine machine) {
        machine.setStatus(new Ordering());
    }

    @Override
    public void prev(CoffeeMachine machine) {
        System.out.println("This status is unavaible!");

    }

    @Override
    public void printStatus() {
        System.out.println("Ready!");
    }
}
