package Coffee;

public class Payment implements CoffeeMachineStatus {

    @Override
    public void next(CoffeeMachine machine) {
        System.out.println("This status is unavaible!");
    }

    @Override
    public void prev(CoffeeMachine machine) {
        machine.setStatus(new Serving());
    }

    @Override
    public void printStatus() {
        System.out.println("Payment!");

    }
}
