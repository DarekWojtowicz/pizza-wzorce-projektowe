package Coffee;

public class CoffeeMachineExecutor {
    public static void main(String[] args) {

        CoffeeMachine jura = new CoffeeMachine();
        jura.printStatus();


        jura.nextState();
        jura.printStatus();

        jura.nextState();
        jura.printStatus();

        jura.nextState();
        jura.printStatus();

        jura.nextState();
        jura.printStatus();

        jura.previousState();
        jura.printStatus();

        jura.previousState();
        jura.printStatus();

        jura.previousState();
        jura.printStatus();

        jura.previousState();
        jura.printStatus();

    }
}
