package Coffee;

public interface CoffeeMachineStatus {

    void next(CoffeeMachine machine);
    void prev (CoffeeMachine machine);
    void printStatus();
}
