package Coffee;

public class Serving implements CoffeeMachineStatus{

    @Override
    public void next(CoffeeMachine machine) {
        machine.setStatus(new Payment());
    }

    @Override
    public void prev(CoffeeMachine machine) {
        machine.setStatus(new Ordering());
    }

    @Override
    public void printStatus() {
        System.out.println("Serving!");
    }
}
