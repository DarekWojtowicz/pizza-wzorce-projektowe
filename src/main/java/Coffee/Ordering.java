package Coffee;

public class Ordering implements CoffeeMachineStatus {


    @Override
    public void next(CoffeeMachine machine) {
        machine.setStatus(new Serving());
    }

    @Override
    public void prev(CoffeeMachine machine) {
        machine.setStatus(new Ready());
    }

    @Override
    public void printStatus() {
        System.out.println("Ordering!");
    }
}
