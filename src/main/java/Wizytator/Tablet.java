package Wizytator;

public class Tablet implements ShopItem{
    double price;
    String name;

    public Tablet(double price, String name) {
        this.price = price;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tablet{" +
                "price=" + price +
                ", name='" + name + '\'' +
                '}';
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }


    @Override
    public double makeDiscount(DiscountMaker visitor) {
        return visitor.visit(this);

    }
}
