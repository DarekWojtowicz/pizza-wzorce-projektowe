package Wizytator;

public interface ShopItem {
    public double makeDiscount(DiscountMaker visitor);
}
