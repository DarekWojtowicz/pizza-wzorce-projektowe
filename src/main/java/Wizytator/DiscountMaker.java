package Wizytator;

public interface DiscountMaker {
    double visit(Lodowka lodowka);
    double visit(Tablet tablet);
}
