package Wizytator;

public class App {
    public static void main(String[] args) {
        Tablet tablet1 = new Tablet(1200, "samsung");
        Tablet tablet2 = new Tablet(499, "No name");
        Lodowka lodowka1 = new Lodowka(3500, "Polar");
        Lodowka lodowka2 = new Lodowka(2999, "Samsung");

        ShopItem[] items = new ShopItem[]{tablet1, tablet2, lodowka1,lodowka2};
        DiscountMaker discount = new MyFirstDiscount();

        double sum = 0;
        for(ShopItem item : items){
            sum = sum + item.makeDiscount(discount);
        }
        System.out.println(sum);

        System.out.println("Before discount: ");
        System.out.println(tablet1.getPrice());
        System.out.println(tablet2.getPrice());
        System.out.println(lodowka1.getPrice());
        System.out.println(lodowka2.getPrice());


        System.out.println("After discount: ");
        System.out.println(tablet1.makeDiscount(discount));
        System.out.println(tablet2.makeDiscount(discount));
        System.out.println(lodowka1.makeDiscount(discount));
        System.out.println(lodowka2.makeDiscount(discount));
    }
}
