package Wizytator;

public class MyFirstDiscount implements DiscountMaker {
    @Override
    public double visit(Lodowka lodowka) {
        double newPrice = lodowka.getPrice();
        if(lodowka.getPrice() > 3000){
            newPrice = lodowka.getPrice() * 0.9;
        }
        return newPrice;
    }

    @Override
    public double visit(Tablet tablet) {
        double newPrice;
        int discount;
        discount = (int) tablet.getPrice() / 500;
        newPrice = tablet.getPrice() - discount * 50;
        return newPrice;
    }
}
