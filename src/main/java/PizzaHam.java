public class PizzaHam extends PizzaDecorator {

    public PizzaHam(Pizza pizza) {
        super(pizza);
    }
    @Override
    public void createPizza() {
        super.createPizza();
        System.out.println("with ham");
    }
}
